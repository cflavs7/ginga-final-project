Atividade Final da Capacitação Ginga 

Seleção LAVID - Edital 1/2017

Desenvolva uma aplicação NCL que obtém informações de tempo de cidades através da Web. A aplicação deve imprimir a temperatura e desenhar um ícone de acordo com esta temperatura.

Parte 1- Uso de mídias (vídeo, imagem e texto) e ancoras.

Escreva uma aplicação que inicia a exibição de um vídeo (video principal) e um ícone de interatividade (i) após (5) segundos de início do vídeo (
Cena 1), após isso exibe o valor da temperatura de São Paulo (GRU) aos 10 segundos (Cena 2), o valor da temperatura de Porto Alegre (POA) aos 15
segundos (Cena 3), o valor da temperatura de João Pessoa (JPA) aos 20 segundos (Cena 4) e ao final, no momento de 25 segundos a aplicação exibe
novamente o ícone da interatividade (i).
Observações:
	1. Utilize mídias de texto (arquivos txt com o nome da cidade e valor da temperatura) para exibir as informações sobre tempo.

Parte 2 - Interatividade e navegação por teclas direcionais

Altere a aplicação da Parte 1 para fazer com que usuário ao clicar no botão BLUE a aplicação exiba a temperatura da cidade GRU (Cena 2). A partir
daí se o usuário clicar na tecla da seta para a direita (CURSOR_RIGHT) ele vai para a temperatura seguinte (no caso, iria para a temperatura de JPA-
Cena 3) , e se clicar na tecla da seta para a esquerda (CURSOR_LEFT) exibe a temperatura anterior. Em qualquer uma das cenas se o usuário clicar
na tecla RED, a aplicação retorna para o ícone de interatividade (Cena 1).

Observações:
	1. Para capturar as teclas direcionais, utilize conectores onSelection ou onKeySelection

Parte 3 - Interatividade e navegação por teclas numéricas utilizando conectores

Altere a aplicação da Parte 1 para fazer com que usuário ao clicar no botão BLUE a aplicação exiba um menu com três opções, um para cada cidade(
Cena 2). Se o usuário escolher um dos números das opções, a aplicação exibe a temperatura da cidade escolhida (Cena 3) e, caso o usuário, digite o
botão RED o menu deve sumir e aplicação exibir novamente o ícone de interatividade. Na Cena 3, se o usuário digitar a tecla GREEN, a aplicação
exibe novamente a tela com o menu de opções (Cena 2), se ele digitar RED, a aplicação exibe novamente o ícone de interatividade (Cena 1)

Observações:
1. Utilize três conectores onKeySelection para capturar o evento das teclas númericas. Não precisa utilizar atributos de foco de descritores.
2. Utilize mídias de texto para exibir as informações sobre tempo.
3. Nesta aplicação, se o usuário voltar ao ícone de interatividade deve ser possível acessar novamente menu.
4. Utilize uma mídia de uma figura (ver abaixo) para exibir o fundo do menu.

Parte 4 - Interatividade e navegação por teclas numéricas utilizando descritores
Altere a aplicação da Parte 2 para fazer com o menu de opção de cidades possa ser navegado por meio de teclas direcionais representando cada opção como botões que
possuem foco. Ao pressionar a tecla ENTER a aplicação deve exibir a temperatura da cidade de acordo com o botão selecionado.
Observações:
1. Edite a figura do menu acima para que cada opção seja representada por uma imagem. Atribua um descritor e região diferente para cada imagem.
2. Utilize os atributos de descritores vistos no Exemplo 6c para tornar as imagens navegaveis por setas direcionais para cima e para baixo.
Para isso voce deve editar a imagem acima e fazer com que o menu seja composto por tres imagens.
Parte 5 - Adaptando o comportamento da aplicação
Utilize variáveis globais, descriptorSwitch ou switch para alem de exibir o valor da temperatura de um arquivo "txt" também exibir embaixo do nome
da cidade e valor da temperatura um ícone (figura png) indicando a situação do tempo (sol, chuva, neve e etc.) de acordo com as seguintes regras (ver
figura ao final da pagina):
Se: tempetura > 22 então SOL
Se: 22 >= temperatura >= 10 então CHUVA
Se: temperatura < 10 então NEVE
No caso, a aplicação deve adaptar a exibição do ícone que indica o tempo de acordo com tres variáveis globais indicando três situações diferentes para
cada cidade. Esses valores são estáticos.
