
  // ensure the web page (DOM) has loaded
  document.addEventListener("DOMContentLoaded", function () {
     // Create a popcorn instance by calling Popcorn("#id-of-my-video")
  var pop = Popcorn("#mainvideo");

   //add an image when the video starts, and ends at 4secs
   //Introduction
   pop.image({
       // 0 seconds
       start: 0,
       // seconds
       end: 4,
       src: "media/ted.png",
       target: "imagediv"
     });

   // Part 2
   pop.image({
   // seconds
   start: 6,
   // seconds
   end: 120,
   src: "media/part2.png",
   target: "imagediv"
  });

  //Part 3
  pop.image({
  // seconds
  start: 124,
  // seconds
  end: 300,
  src: "media/part3.png",
  target: "imagediv"
 });
 //Part 4
 pop.image({
 // seconds
 start: 500,
 // seconds
 end: 600,
 src: "media/part4.png",
 target: "imagediv"
});

// play the video right away
pop.play();

// Change menu item colours  according to video time
pop.listen( "timeupdate", function() {
       var time = this.currentTime();
       if(time<4){
         document.getElementById('intro').style.color = "red"
         document.getElementById('part2').style.color = "black"
         document.getElementById('part3').style.color = "black"
         document.getElementById('part4').style.color = "black"
         document.getElementById('part5').style.color = "black"
       }
       else if (time>6 && time<120) {
         document.getElementById('part2').style.color = "red"
         document.getElementById('intro').style.color = "black"
         document.getElementById('part3').style.color = "black"
         document.getElementById('part4').style.color = "black"
         document.getElementById('part5').style.color = "black"
       }
       else if (time>120 && time<300) {
         document.getElementById('part3').style.color = "red"
         document.getElementById('intro').style.color = "black"
         document.getElementById('part2').style.color = "black"
         document.getElementById('part4').style.color = "black"
         document.getElementById('part5').style.color = "black"
       }
       else if (time>500 && time<684) {
         document.getElementById('part4').style.color = "red"
         document.getElementById('intro').style.color = "black"
         document.getElementById('part2').style.color = "black"
         document.getElementById('part3').style.color = "black"
         document.getElementById('part5').style.color = "black"
       }
       else if (time>=684) {
         document.getElementById('part5').style.color = "red"
         document.getElementById('intro').style.color = "black"
         document.getElementById('part2').style.color = "black"
         document.getElementById('part3').style.color = "black"
         document.getElementById('part4').style.color = "black"
       }
   });

   // Shows form when video ends
   pop.listen( "timeupdate", function() {
       console.log(this.currentTime());
       var time = this.currentTime();
       if(time>=684){
         console.log('acabou');
         var form = document.getElementById('form');
         form.style.display = 'block';
         var img = document.getElementById('imagemfinal');
         img.style.display = 'block';
       }
       else {
         var form = document.getElementById('form');
         form.style.display = 'none';
         var img = document.getElementById('imagemfinal');
         img.style.display = 'none';
       }
   });

   //When menu item is selected, video jumps to the begining of item section

   document.getElementById('intro').addEventListener('click', function () {
     pop.currentTime(0);
   }, false);

   document.getElementById('part2').addEventListener('click', function () {
     pop.currentTime(6);
   }, false);

   document.getElementById('part3').addEventListener('click', function () {
     pop.currentTime(124);
   }, false);

   document.getElementById('part4').addEventListener('click', function () {
     pop.currentTime(500);
   }, false);
   document.getElementById('part5').addEventListener('click', function () {
     pop.currentTime(684);
   }, false);


  }, false);

function myFunction() {
 var x = document.getElementById('mainvideo');
 if (x.style.display === 'none') {
     x.style.display = 'block';
 } else {
     x.style.display = 'none';
 }
}
