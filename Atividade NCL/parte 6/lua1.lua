
PROPRIEDADE_NOME = 'numero'
numeros = {'1','2','3','4'}

function handler(e)

	if (e.class == 'ncl' and e.type == 'presentation' and e.action=='start') then



		-- escolhe um numero entre 1 e 4 e guarda em 'numeroAleatorio'

		local index = ((event.uptime() + os.time()) % 4 ) + 1
		local numeroAleatorio = numeros[ index ]

		print('Numero gerado:', numeroAleatorio) io.flush()

		-- gera evento de atribuicao
		geraEventoDeAtriuicao(PROPRIEDADE_NOME, numeroAleatorio)

	end
end


function geraEventoDeAtriuicao(nomePropriedade, valor)

		evt = {
			class    = 'ncl',
			type     = 'attribution',
			action   = 'start',
			name	 = nomePropriedade,
			value    = valor,
		}

		event.post(evt) -- evento de inicio da atribuicao
		evt.action = 'stop'
		event.post(evt) -- evento de final da atribuicao

		print('Evento de atribuicao gerado...') io.flush()
end



event.register(handler)
