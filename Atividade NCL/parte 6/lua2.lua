PROPRIEDADE_NOME = 'numero'
local open = io.open

local function read_file(path)
    local file = open(path, "rb") -- r read mode and b binary mode
    if not file then return nil end
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
end

local fileContent = read_file("media/texto/jpa.txt");
print (fileContent);

objProp = {}
index = 1
for value in string.gmatch(fileContent,"%w+") do 
    objProp [index] = value
    index = index + 1
end
print(objProp[2])
 
function handler(e)

	if (e.class == 'ncl' and e.type == 'presentation' and e.action=='start') then



		-- escolhe um numero entre 1 e 4 e guarda em 'numeroAleatorio'

		local temp = tonumber(objProp[2]);

		print('Temperatura:', objProp[2]) io.flush()

		if temp > 22 then
		-- gera evento de atribuicao
		  geraEvento(PROPRIEDADE_NOME, 1)
		elseif (temp <= 22 and temp >= 10) then
		  geraEvento(PROPRIEDADE_NOME, 2)
		elseif temp < 10 then
		  geraEvento(PROPRIEDADE_NOME, 3)
		end 

	end
end

function geraEvento(nomePropriedade, valor)

		evt = {
			class    = 'ncl',
			type     = 'attribution',
			action   = 'start',
			name	 = nomePropriedade,
			value    = valor,
		}

		event.post(evt) -- evento de inicio da atribuicao
		evt.action = 'stop'
		event.post(evt) -- evento de final da atribuicao

		print('Evento de atribuicao gerado...') io.flush()
end

event.register(handler)
