<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Generated	by NCL Eclipse -->
<ncl id="connectorBase" xmlns="http://www.ncl.org.br/NCL3.0/EDTVProfile">
	<head>
		<connectorBase>
			<causalConnector id="onBeginStart">
				<simpleCondition role="onBegin" />
				<simpleAction role="start" />
			</causalConnector>
			
			<causalConnector id="onBeginStop">
				<simpleCondition role="onBegin" />
				<simpleAction role="stop" />
			</causalConnector>
			
			<causalConnector id="onBeginStart_vDelay">
				<connectorParam name="vDelay"/>
				<simpleCondition role="onBegin" />
				<compoundAction operator="par" delay="$vDelay">
					<simpleAction role="stop" max="unbounded" />
					<simpleAction role="start" max="unbounded" />	
				</compoundAction>
			</causalConnector>
			
			<causalConnector id="onKeySelectionStopStart">
				<connectorParam name="vKey"/>
				<simpleCondition role="onSelection" key="$vKey"/>
				<compoundAction operator="par">
					<simpleAction role="stop" max="unbounded" />
					<simpleAction role="start" max="unbounded" />	
				</compoundAction>
			</causalConnector>
			
			<causalConnector id="onKeySelectionStop">
				<connectorParam name="vKey"/>
				<simpleCondition role="onSelection" key="$vKey"/>
				<simpleAction role="stop" max="unbounded" />
			</causalConnector>	
			<causalConnector id="onKeySelectionStart">
				<connectorParam name="vKey"/>
				<simpleCondition role="onSelection" key="$vKey"/>
				<simpleAction role="start" max="unbounded" />
			</causalConnector>
			
			<causalConnector id="onEndAttributionSet">
				<connectorParam name="value"/>
				<simpleCondition role="onEndAttribution"/>
				<simpleAction role="set" value="$value" max="unbounded" qualifier="par"/>
			</causalConnector>
			
			<causalConnector id="onBeginSet">
				<connectorParam name="value"/>
				<simpleCondition role="onBegin"/>
				<simpleAction role="set" value="$value" max="unbounded" qualifier="par"/>
			</causalConnector>	
			<causalConnector id="onEndSet">
				<connectorParam name="value"/>
				<simpleCondition role="onEnd"/>
				<simpleAction role="set" value="$value" max="unbounded" qualifier="par"/>
			</causalConnector>
			
			<causalConnector id="onBeginStartN">
				<simpleCondition role="onBegin"/>
				<simpleAction role="start" max="unbounded" qualifier="par"/>
			</causalConnector>
			
			<causalConnector id="onBeginStopN">
				<simpleCondition role="onBegin"/>
				<simpleAction role="stop" max="unbounded" qualifier="par"/>
			</causalConnector>
			
			<causalConnector id="onEndStart">
				<simpleCondition role="onEnd"/>
				<simpleAction role="start
				" max="unbounded" qualifier="par"/>
			</causalConnector>
			
			<causalConnector id="onSelectionStart">
				<simpleCondition role="onSelection"/>
				<simpleAction role="start" value="$value" max="unbounded" qualifier="par"/>
			</causalConnector>
			
			<causalConnector id="onSelectionStartStop">
				<simpleCondition role="onSelection"/>
				<compoundAction operator="par">
					<simpleAction role="start" value="$value" max="unbounded" qualifier="par"/>
					<simpleAction role="stop" value="$value" max="unbounded" qualifier="par"/>				
				</compoundAction>
			</causalConnector>
		</connectorBase>
	</head>
</ncl>