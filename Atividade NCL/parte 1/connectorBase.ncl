<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Generated	by NCL Eclipse -->
<ncl id="connectorBase" xmlns="http://www.ncl.org.br/NCL3.0/EDTVProfile">
	<head>
		<connectorBase>
			<!-- Start midia on begin -->
			<causalConnector id="onBeginStart">
				<simpleCondition role="onBegin" />
				<simpleAction role="start" />
			</causalConnector>
			<!-- Stop midia on begin -->
			<causalConnector id="onBeginStop">
				<simpleCondition role="onBegin" />
				<simpleAction role="stop" />
			</causalConnector>
			<!-- Start and Stop midias with a Delay-->
			<causalConnector id="onBeginStartStop_vDelay">
				<connectorParam name="vDelay"/>
				<simpleCondition role="onBegin" />
				<compoundAction operator="par" delay="$vDelay">
					<simpleAction role="stop" max="unbounded" />
					<simpleAction role="start" max="unbounded" />	
				</compoundAction>
			</causalConnector>	
		</connectorBase>
	</head>
</ncl>